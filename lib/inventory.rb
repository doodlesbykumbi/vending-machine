module VendingMachine
	class Inventory
		attr_reader :product_capacity
		attr_accessor :products, :total

		PRODUCT_TYPES = {
			"kit-kat" => 0.8,
			"water" => 2,
			"smoothie" => 3
		}

		def initialize(product_capacity = 100, product_load = 100)
			# TODO: add logic to ensure product_load is less than capacity
			@product_capacity = product_capacity

			@products = PRODUCT_TYPES.map { |name, _| [name, product_load] }.to_h
			@total = product_load * PRODUCT_TYPES.keys.count
		end

		def price!(name)
			raise(Exception, "#{name} is not offered, sorry") unless PRODUCT_TYPES.key?(name)

			PRODUCT_TYPES[name]
		end

		def count!(name)
			raise(Exception, "#{name} is not offered, sorry") unless PRODUCT_TYPES.key?(name)

			@products[name]
		end

		def release!(number, name)
			raise(Exception, "#{name} is not offered, sorry") unless PRODUCT_TYPES.key?(name)

			product_items_count = @products[name]

			raise(Exception, "#{name} not in stock, sorry") if product_items_count == 0
			@products[name] -= number

			@total -= number
		end

		def receive!(number, name)
			raise(Exception, "#{name} is not offered, sorry") unless PRODUCT_TYPES.key?(name)

			product_items_count = @products[name]

			raise(Exception, "#{name} holder full, sorry") if product_items_count + number > @product_capacity
			@products[name] += number

			@total += number
		end
	end
end
