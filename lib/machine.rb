require 'bigdecimal'

module VendingMachine
	class Machine
		attr_reader :inventory, :till, :current_budget, :coins_inserted
		attr_accessor :return_change

		def initialize( product_capacity = 100, product_load = 100, coin_capacity = 100, coin_load = 20)
			@inventory = Inventory.new(product_capacity, product_load)
			@till = Till.new(coin_capacity)

			Till::ACCEPTED_COINS.each { |coin| till.coins_count[coin] += coin_load }

			@current_budget = 0
			@coins_inserted = []
		end

		def insert!(coin)
			@current_budget += coin
			@coins_inserted << coin
		end

		def order_product!(product_name)
			price = inventory.price!(product_name)

			money_gap = price - @current_budget
			raise(Exception, "To purchase this product please insert an extra £#{money_gap}.") if money_gap > 0
			change = return_change(product_name)
			@inventory.release!(1, product_name)
			@coins_inserted.each do |coin|
				till.accept!(coin)
			end
			@current_budget = 0

			[product_name, change]
		end

private
		def return_change(product_name)
			price = inventory.price!(product_name)

			@return_change = []
			amount_owed = @current_budget - price
			if amount_owed <= 0
				return @return_change
			end

			matches = till.withdraw_combination(amount_owed)
			raise(Exception, "Unable to provide change for #{amount_owed}.") if matches == nil

			matches.each do |match|
				coin, count = match
				count.times {
					return_coin(coin)
				}
			end

			@return_change
		end

		def return_coin(value)
			@till.return!(value)
			@return_change << value
		end
	end
end
