module VendingMachine
	class Till
		attr_accessor :coins_count, :total, :capacity

		ACCEPTED_COINS = [0.01, 0.02, 0.05, 0.1, 0.2, 0.5 , 1, 2]

		def initialize(capacity = 100)
			@capacity = capacity
			@total = 0
			@coins_count = ACCEPTED_COINS.map { |coin| [coin, 0] }.to_h
		end

		def count!(coin)
			raise(Exception, "#{coin} coins are not accepted, sorry") unless ACCEPTED_COINS.include?(coin)

			@coins_count[coin]
		end

		def withdraw(number, name)
			raise(Exception, "#{name} is not offered, sorry") unless PRODUCT_TYPES.key?(name)

			product_items_count = @products[name]

			raise(Exception, "#{name} not in stock, sorry") if product_items_count == 0
			@products[name] -= number

			@product_load -= number
		end

		def accept!(coin)
			raise(Exception, "#{coin} coins are not accepted, sorry") unless ACCEPTED_COINS.include?(coin)

			coin_count = @coins_count[coin]

			raise(Exception, "#{coin} pound(s) coin holder full") if coin_count + 1 > capacity

			@coins_count[coin] += 1
			@total += coin
		end

		def return!(coin)
			raise(Exception, "#{coin} coins are not accepted, sorry") unless ACCEPTED_COINS.include?(coin)

			coin_count = @coins_count[coin]

			raise(Exception, "#{coin} pound(s) coin holder empty") if coin_count == 0

			@coins_count[coin] -= 1
			@total -= coin
		end

		def withdraw_combination(amount)
			if amount == 0
				return nil
			end

			coins = coins_count.keys
									.sort
									.reverse
									.map { |k| [k, coins_count[k]] }
									.map {|v| [v[0], v[1]]}

			_calculateWithdrawCombination(coins, amount)
		end

		private

		# iteratively check for viable candidate, biggest to lowest
		# ref: http://putridparrot.com/blog/the-vending-machine-change-problem/
		def _calculateWithdrawCombination(coins, change, start = 0)
			i = start
			change = BigDecimal("#{change}")
			while i < coins.count
				_coin, count = coins[i]
				coin = BigDecimal("#{_coin}")

				if count > 0 && coin <= change

					_, remainder = change.divmod(coin)
					if remainder < change
						howMany = [count, (change - remainder).divmod(coin)[0]].min

						matches = []
						matches << [_coin, howMany.to_i]

						amount = howMany * coin
						changeLeft = change - amount

						if changeLeft == 0
							return matches;
						end

						subCalc = _calculateWithdrawCombination(coins, changeLeft, i + 1)
						if subCalc != nil
							matches.push(*subCalc)
							return matches
						end
					end
				end

				i += 1
			end

			return nil
		end

	end
end
