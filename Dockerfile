FROM ruby:2.3.7
RUN mkdir /src
COPY ./Gemfile** /src/
WORKDIR /src
RUN bundle install
