require 'till'

include VendingMachine

describe Till do
	let(:till) { Till.new(20) }

	it 'can accept a coin' do
		till.accept!(1)
		expect(till.total).to eq 1
		expect(till.count!(1)).to eq 1
	end

	it "can accept multiple coins" do
		till.accept!(1)
		till.accept!(0.02)
		till.accept!(0.02)

		expect(till.total).to eq 1.04
		expect(till.count!(1)).to eq 1
		expect(till.count!(0.02)).to eq 2
		expect(till.count!(0.01)).to eq 0
	end

	it 'can return a coin' do
		till.accept!(1)
		till.return!(1)
		till.accept!(0.02)

		expect(till.total).to eq 0.02
		expect(till.count!(1)).to eq 0
		expect(till.count!(0.02)).to eq 1
	end

	it 'has a default capacity of 100 for each coin type' do
		till = Till.new
		expect(till.capacity).to eq 100
	end
	
	it 'knows when one of its holders is full' do
		i = 20
		while i > 0 do
			till.accept!(1)
			i -=1
    end

		expect{ till.accept!(1) }.to raise_error(Exception, "1 pound(s) coin holder full")
	end

	it 'knows when one of its holders is empty' do
		expect{ till.return!(1) }.to raise_error(Exception, "1 pound(s) coin holder empty")
	end
end
