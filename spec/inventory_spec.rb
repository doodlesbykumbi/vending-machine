require 'inventory'

include VendingMachine

describe Inventory do
	let(:inventory) { Inventory.new(50, 15) }

	context "on initialisation" do

		it 'has a default product capacity of 100' do
      inventory = Inventory.new

			expect(inventory.product_capacity).to eq 100
		end

		it 'has a default products load of 100 items' do
			inventory = Inventory.new

      # 100 * 3 types = 300
			expect(inventory.total).to eq 300
		end

		it 'has an equal quantity of kit-kats, water and smoothies' do
			expect(inventory.count!("kit-kat")).to eq 15
			expect(inventory.count!("water")).to eq 15
			expect(inventory.count!("smoothie")).to eq 15
		end

		it 'has an equal quantity of each type of product, even when product load not default' do
			small_inventory = Inventory.new(40, 5)

			expect(small_inventory.count!("kit-kat")).to eq 5
			expect(small_inventory.count!("water")).to eq 5
			expect(small_inventory.count!("smoothie")).to eq 5
		end
	end

	context 'basic functionality' do
		it 'knows how many products it has' do
			expect(inventory.total).to eq 45
		end

		it 'can release a product' do
			inventory.release!(1, "water")

			expect(inventory.total).to eq 44
			expect(inventory.count!("water")).to eq 14
		end

		it 'can receive a product' do
			inventory.release!(6, "water")
			inventory.receive!(3, "water")

			expect(inventory.total).to eq 42
			expect(inventory.count!("water")).to eq 12
		end
	end

	context 'error messages' do
		it 'cannot receive items not offered' do
			expect { inventory.receive!(1, "uranium") }.to raise_error(Exception, "uranium is not offered, sorry")
    end

		it 'cannot release items not offered' do
			expect { inventory.release!(1, "uranium") }.to raise_error(Exception, "uranium is not offered, sorry")
		end

		it 'cannot receive more items than its capacity' do
			expect { inventory.receive!(100, "water") }.to raise_error(Exception, "water holder full, sorry")
		end

		it "cannot release items when it's empty" do
			empty_inventory = Inventory.new(100, 0)

			expect { empty_inventory.release!(1, "water") }.to raise_error(Exception, "water not in stock, sorry")
		end
	end
end
