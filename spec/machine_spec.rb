require 'machine'

include VendingMachine

describe Machine do
	let(:machine) {
    Machine.new(60, 55, 50, 15)
  }

	context "initialisation" do
		it 'loads the vending machine with specified number of products on initialisation' do
			expect(machine.inventory.total).to eq 55 * 3
		end

		it "loads the till with specified number of coins of each type on initialisation" do
			expect(machine.till.count!(0.5)).to eq 15
			expect(machine.till.count!(2)).to eq 15
		end
	end

	context "purchase" do
		it "knows the total amount the client has inserted" do 
			machine.insert!(1)
			machine.insert!(0.5)

			expect(machine.current_budget).to eq 1.5
		end

		it "allows the client to purchase a product" do
			machine.insert!(2)
			machine.order_product!("water")

			expect(machine.inventory.total).to eq 164
    end

		it "returns the requested product" do
			machine.insert!(2)
			product, _ = machine.order_product!("water")

			expect(product).to eq "water"
		end

		it "updates the inventory count for that type of product" do
			machine.insert!(2)
			machine.order_product!("water")

			expect(machine.inventory.count!("water")).to eq 54
		end

		it "displays an error message if budget is too low to buy product" do
			machine.insert!(1)

			expect{ machine.order_product!("water") }.to raise_error(Exception)
		end

		it "displays amount needed if budget is too low to buy product" do
			machine.insert!(1)

			expect{ machine.order_product!("water") }.to raise_error(Exception, "To purchase this product please insert an extra £1.")
		end
	end

	context "after purchase" do
		it "adds the coins the clients has inserted to the till" do
			machine.insert!(1)
			machine.insert!(1)
			machine.order_product!("water")

			expect(machine.till.count!(1)).to eq 17
		end

		it "returns nothing when there is no coin to return" do
			product = "water"

			machine.insert!(2)
			_, change = machine.order_product!(product)

			expect(machine.till.count!(0.5)).to eq 15
			expect(machine.till.count!(2)).to eq 16
			expect(change.count).to eq 0
    end

		it "returns change when there is one coin to return" do
			product = "smoothie"

			machine.insert!(2)
			machine.insert!(2)
			_, change = machine.order_product!(product)

			expect(machine.till.count!(0.5)).to eq 15
			expect(machine.till.count!(2)).to eq 17
			expect(change.count).to eq 1
			expect(change[0]).to eq 1
		end

		it "returns change when there are 2 coins to return" do
			product = "kit-kat"

			machine.insert!(2)
			_, change = machine.order_product!(product)

			expect(machine.till.count!(0.20)).to eq 14
			expect(machine.till.count!(1)).to eq 14
			expect(machine.till.count!(2)).to eq 16
			expect(change.count).to eq 2
			expect(change[0]).to eq 1
			expect(change[1]).to eq 0.2
    end

		it "returns change when there are multiple coins to return" do
			machine = Machine.new(60, 75, 50, 0)
			product = "kit-kat"

      machine.till.accept!(0.2)
      machine.till.accept!(0.2)
      machine.till.accept!(0.5)
      machine.till.accept!(0.5)

			machine.insert!(2)
			_, change = machine.order_product!(product)

			expect(machine.till.count!(0.20)).to eq 1
			expect(machine.till.count!(1)).to eq 0
			expect(change.count).to eq 3
			expect(change[0]).to eq 0.5
			expect(change[1]).to eq 0.5
			expect(change[2]).to eq 0.2
		end
	end
end

