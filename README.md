# Vending Machine

## Motivation

Programming Exercise: Vending Machine Exercise

Design a vending machine using ruby. The vending machine should perform as follows:

Once an item is selected and the appropriate amount of money is inserted, the vending machine should return the correct product.
It should also return change if too much money is provided, or ask for more money if insufficient funds have been inserted.
The machine should take an initial load of products and change. The change will be of denominations 1p, 2p, 5p, 10p, 20p, 50p, £1, £2.
There should be a way of reloading either products or change at a later point.
The machine should keep track of the products and change that it contains.

## Usage

For the moment, VendingMachine takes life inside a Ruby program.
To access this in an interactive Ruby shell run the following:

```
irb -r ./entrypoint.rb
```

Create a vending machine using positional arguments to specify initial load of products and coins i.e. product_capacity, product_load, coin_capacity, coin_load.
```
v = VendingMachine::Machine.new(60, 20, 50, 15)
```
```
=> #<VendingMachine::Machine:0x000055f2f00960a8 
@inventory=#<VendingMachine::Inventory:0x000055f2f0096030 
@product_capacity=60, @products={"kit-kat"=>20, "water"=>20, "smoothie"=>20}, @total=60>, 
@till=#<VendingMachine::Till:0x000055f2f0095ae0 @capacity=50, @total=0, 
@coins_count={0.01=>15, 0.02=>15, 0.05=>15, 0.1=>15, 0.2=>15, 0.5=>15, 1=>15, 2=>15}>, @current_budget=0, @coins_inserted=[]>
```

Insert some money (perhaps too much)
```
v.insert! 2
v.insert! 1
v.insert! 1
```

Check inventory
```
v.inventory.products
```
```
=> {"kit-kat"=>20, "water"=>20, "smoothie"=>20}
```

Order a product
```
output, change = v.order_product! "water"
```
```
=> ["water", [2]]
```

Keep track of inventory

```
v.inventory.products
```
```
=> {"kit-kat"=>20, "water"=>19, "smoothie"=>20}
```

Update inventory
```
v.inventory.receive(5, "water")
v.inventory.products
```
```
=> {"kit-kat"=>20, "water"=>24, "smoothie"=>20}
```

Monitor coin change
```
v.till.coins_count
```
```
=> {0.01=>15, 0.02=>15, 0.05=>15, 0.1=>15, 0.2=>15, 0.5=>15, 1=>17, 2=>15}
```

Add more coin change
```
v.till.accept!(2)
v.till.coins_count
```
```
=> {0.01=>15, 0.02=>15, 0.05=>15, 0.1=>15, 0.2=>15, 0.5=>15, 1=>17, 2=>16}
```

## Prerequisites for development

Before getting started, you should install some developer tools. These are not
required to deploy the VendingMachine but they will let you develop using a standardized environment.

1. [git][get-git] to manage source code

Local:

1. [Ruby][get-ruby]

Using Docker:

2. [Docker][get-docker] to manage dependencies and runtime environments
3. [Docker Compose][get-docker-compose] to orchestrate Docker environments

[get-ruby]: https://www.ruby-lang.org/en/documentation/installation/
[get-docker]: https://docs.docker.com/engine/installation
[get-git]: https://git-scm.com/downloads
[get-docker-compose]: https://docs.docker.com/compose/install

## Set up a development environment

The `dev` directory contains a `docker-compose` file which creates a development
environment comprising a
`dev` container with source code mounted into the directory
`/src`.

To use it:

1. Install dependencies (as above)
2. Start the container:

   ```sh-session
   $ cd scripts
   $ ./dev.sh
   ...
   root@f39015718062:/src#
   ```

   Once the `dev.sh` script finishes, you're in a Bash shell inside the `dev` container. In this container you will have access to `Ruby 2.3.7` and all the `Gemfile` dependencies will be installed.

## Testing

VendingMachine has `rspec` tests.

### RSpec

RSpec tests are easy to run locally or within a `dev` container:

```sh-session
root@aa8bc35ba7f4:/src# rspec

VendingMachine::Inventory
  on initialisation
    has a default product capacity of 100
    has a default products load of 100 items

.............................................

Finished in 0.01626 seconds (files took 0.1357 seconds to load)
30 examples, 0 failures

```
