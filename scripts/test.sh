#!/usr/bin/env bash

cd $(dirname $0)/..;

export COMPOSE_FILE=./dev/docker-compose.yml
docker-compose build
docker-compose run dev rspec
